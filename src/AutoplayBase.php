<?php

namespace Drupal\autoplay;

use \Drupal\webform\Entity\WebformSubmission;

class AutoplayBase {

  public static function handleSubmission($sid, $webform_config) {
    $submission = WebformSubmission::load($sid);
    if (!$submission) {
      return;
    }

    $webform = $submission->getWebform();
    $errors = [];

    // Determine if to create mutliple leads from one submission.
    if (!empty($webform_config['run_multiple'])) {
      $submission_values = $submission->toArray(TRUE);
      if (!empty($submission_values['data'][$webform_config['run_multiple']])) {
        $multiple_selection = $submission_values['data'][$webform_config['run_multiple']];
        foreach ($multiple_selection as $index => $value) {
          $idx = count($multiple_selection) > 1 ? $index : NULL;
          $errors[] = self::buildAndMakeRequest($webform_config, $submission, $idx);
        }
      }
    }
    else {
      $errors[] = self::buildAndMakeRequest($webform_config, $submission);
    }

    if ($webform_config['purging'] && empty($errors)) {
      $submission->delete();
    }

    $errors = array_filter($errors);

    $error = implode("\n", $errors);
    // @todo Review logging needs.
    $log = [
      '@form' => $webform->label(),
      '@function' => $webform_config['operation'],
      '@error' => $error,
    ];
    if (!empty($error)) {
      \Drupal::messenger()->addError($error);
      \Drupal::logger('autoplay')->error('@form @function - @error', $log);
    }
  }

  public static function handleIncrementalToken(&$token, $index) {
    if (is_int($index)) {
      $token = str_replace(':X:', ":$index:", $token);
    } else {
      $token = str_replace(':X:', ':', $token);
    }
  }

  public static function buildAndMakeRequest($webform_config, $submission, $idx = NULL) {
    $arguments = self::buildRequest($webform_config, $submission, $idx);
    self::handleOverrides($arguments, $webform_config);
    $error = self::makeRequest($arguments, $webform_config);
    return $error;
  }

  public static function buildRequest($webform_config, $submission, $idx = NULL) {
    $webform = $submission->getWebform();
    $token_service = \Drupal::token();

    $operation = $webform_config['operation'];
    $arguments[$operation]['request'] = [];

    // Assign properties and process tokens.
    $tokenData = [
      'webform' => $webform,
      'webform_submission' => $submission,
    ];
    $tokenOptions = ['clear' => TRUE];
    foreach ($webform_config['props'] as $prop => $raw) {
      self::handleIncrementalToken($raw, $idx);
      $value = $token_service->replace($raw, $tokenData, $tokenOptions);
      // We cannot send empty strings or AutoPlay will error on validation.
      if ($value) {
        $arguments[$operation]['request'][$prop] = $value;
      }
    }

    return $arguments;
  }

  public static function handleOverrides(&$arguments, $webform_config) {
    $operation = $webform_config['operation'];

    // Test mode.
    if ($webform_config['testmode']) {
      $config = \Drupal::config('autoplay.settings');

      $overrides = [
        'DealershipId' => $config->get('default_apid'),
        'YardId' => $config->get('default_yardid'),
      ];
      foreach ($overrides as $key => $value) {
        $arguments[$operation]['request'][$key] = $value;
      }
    }
  }

  public static function makeRequest($arguments, $webform_config) {
    $operation = $webform_config['operation'];
    $config = \Drupal::config('autoplay.settings');

    // Prepare the authentication block and add it to the header.
    $auth = '<API_KEY>' . $config->get('key') . '</API_KEY>';
    $auth .= '<API_TOKEN>' . $config->get('token') . '</API_TOKEN>';
    $auth_block = new \SoapVar($auth, XSD_ANYXML);
    $header = new \SoapHeader(
      'http://schemas.xmlsoap.org/soap/envelope',
      'authentication',
      $auth_block,
      FALSE);

    try {
      $client = new \SoapClient($webform_config['wsdl'], ['trace' => 1]);
      $client->__setSoapHeaders($header);
    }
    catch (\SoapFault $e) {
      \Drupal::messenger()->addError($e->getMessage());
      return;
    }
    if ($webform_config['debug'] && function_exists('dpm')) {
      dpm($arguments[$operation]['request']);
    }
    $error = NULL;
    $response = $lastRequest = NULL;
    try {
      $response = $client->__soapCall($operation, $arguments);
      $lastRequest = $client->__getLastRequest();

      $error = $response->{$webform_config['response']}->ErrorMessage;
    }
    catch (\SoapFault $e) {
      $error = $e->getMessage();
    }

    if ($webform_config['debug']) {
      \Drupal::messenger()->addMessage('Request: ' . $lastRequest);
      \Drupal::messenger()->addMessage('Response: '.print_r($response, TRUE));
    }
    return $error;
  }

}
