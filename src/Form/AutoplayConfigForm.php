<?php

namespace Drupal\autoplay\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class AutoplayConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'autoplay.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'autoplay_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('autoplay.settings');

    $form['key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $config->get('key'),
      '#required' => TRUE,
    ];

    $form['token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Token'),
      '#default_value' => $config->get('token'),
      '#required' => TRUE,
    ];

    $form['default_apid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default override Autoplay ID'),
      '#default_value' => $config->get('default_apid'),
      '#description' => $this->t('This is used if the test mode on a webform is enabled to ensure all API calls end up in one sandbox.'),
    ];

    $form['default_yardid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default override Yard ID'),
      '#default_value' => $config->get('default_yardid'),
      '#description' => $this->t('This is used if the test mode on a webform is enabled to ensure all API calls end up in one sandbox.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('autoplay.settings')
      ->set('key', $form_state->getValue('key'))
      ->set('token', $form_state->getValue('token'))
      ->set('default_apid', $form_state->getValue('default_apid'))
      ->set('default_yardid', $form_state->getValue('default_yardid'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
