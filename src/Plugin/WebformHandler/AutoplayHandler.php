<?php

namespace Drupal\autoplay\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\autoplay\AutoplayBase;

/**
 * Webform submission SES subscription manager.
 *
 * @WebformHandler(
 *   id = "autoplay_handler",
 *   label = @Translation("AutoPlay"),
 *   category = @Translation("External"),
 *   description = @Translation("Sends data to AutoPlay."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 *   tokens = TRUE,
 * )
 */
class AutoplayHandler extends WebformHandlerBase {

  /**
   * The token handler.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->token = $container->get('token');
    $instance->queue_factory = $container->get('queue');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $submission, $update = TRUE) {
    $isCompleted = ($submission->getState() == WebformSubmissionInterface::STATE_COMPLETED);
    $isUpdated = ($submission->getState() == WebformSubmissionInterface::STATE_UPDATED);
    if (!$isCompleted && !$isUpdated) {
      return;
    }
    elseif (!$this->configuration['resend_on_update'] && $isUpdated) {
      return;
    }

    // Add to queue or else process directly.
    if ($this->configuration['use_queue']) {
      $queue_factory = $this->queue_factory;
      $queue = $queue_factory->get('autoplay_submission_handler');
      $item = (object)[
        'sid' => $submission->id(),
        'config' => $this->configuration
      ];
      $queue->createItem($item);
      \Drupal::logger('autoplay')->notice("Queue item added for " . $submission->id());
    }
    else {
      AutoplayBase::handleSubmission($submission->id(), $this->configuration);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      // @todo Autoplay has test and live endpoints but they do the same thing.
      // https://lead-api.autoplay.co.nz/V2/LeadAPI.svc?singleWsdl
      'wsdl' => 'https://lead-api.aptest.co.nz/LeadAPI.svc?singleWsdl',
      'operation' => 'SaveLead',
      'response' => 'SaveLeadResult',
      'testmode' => FALSE,
      'debug' => FALSE,
      'purging' => FALSE,
      'run_multiple' => '',
      'use_queue' => FALSE,
      'resend_on_update' => FALSE,
      'props' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    try {
      $client = new \SoapClient($this->configuration['wsdl'], ['trace' => 1]);

      $types = $this->parseTypes($client->__getTypes());
      $operations = $this->parseOperations($client->__getFunctions());
    }
    catch (\SoapFault $e) {
      \Drupal::messenger()->addError($e->getMessage());
    }

    $webform = $this->getWebform();
    $elements = $webform->getElementsInitializedAndFlattened();
    $multiples = [];
    foreach ($elements as $key => $element) {
      if ($element['#webform_multiple']) {
        $multiples[$key] = $element['#title'];
      }
    }

    // Remote.
    $form['remote'] = [
      '#type' => 'details',
      '#title' => $this->t('Remote server'),
      '#description' => $this->t('The remote SOAP server configuration details.'),
      '#open' => TRUE,
    ];
    $form['remote']['wsdl'] = [
      '#title' => $this->t('WSDL'),
      '#type' => 'url',
      '#default_value' => $this->configuration['wsdl'],
      '#description' => $this->t('Autoplay endpoint including the full WSDL. The production URL should be <em>https://lead-api.autoplay.co.nz/V2/LeadAPI.svc?singleWsdl</em>.'),
      '#required' => TRUE,
    ];
    $form['remote']['debug'] = [
      '#title' => $this->t('Show debug values?'),
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['debug'],
      '#description' => $this->t("If enabled, data sent and received will be sent to user screen. WARNING: don't enable this in PROD environment."),
      '#required' => FALSE,
    ];
    $form['remote']['testmode'] = [
      '#title' => $this->t('Test mode? (Overrides DealershipId and YardId)'),
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['testmode'],
      '#description' => $this->t("If enabled, the default DealershipId and YardId values will be assigned to all requests. WARNING: don't enable this in PROD environment."),
      '#required' => FALSE,
    ];

    $opKeys = array_keys($operations);
    $form['remote']['operation'] = [
      '#title' => $this->t('Operation'),
      '#type' => 'select',
      '#default_value' => $this->configuration['operation'],
      '#options' => array_combine($opKeys, $opKeys),
      '#description' => $this->t('Select AutoPlay method to process.'),
      '#required' => TRUE,
    ];

    $form['remote']['response'] = [
      '#title' => $this->t('Response Message'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['response'],
      '#description' => $this->t('The response object the operation should return when successful.'),
      '#required' => TRUE,
    ];

    $form['remote']['purging'] = [
      '#title' => $this->t('Submission purging'),
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['purging'],
      '#description' => $this->t('If enabled, the submission will be purged after valid server response.'),
      '#required' => FALSE,
    ];

    if (!empty($multiples)) {
      // TODO? Multiple multiples?? Potentially many permutations/requests
      $form['remote']['run_multiple'] = [
        '#title' => $this->t('Run for each selection'),
        '#type' => 'select',
        '#default_value' => $this->configuration['run_multiple'],
        '#empty_option' => $this->t('- Select -'),
        '#options' => $multiples,
        '#description' => $this->t('Make a separate request for each selection.'),
        '#required' => FALSE,
      ];
    }

    $form['remote']['use_queue'] = [
      '#title' => $this->t('Use queue'),
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['use_queue'],
      '#description' => $this->t('If enabled, the requests will be queued.'),
      '#required' => FALSE,
    ];

    $form['remote']['resend_on_update'] = [
      '#title' => $this->t('Resend on submission edit/update'),
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['resend_on_update'],
      '#description' => $this->t('Resend to AP when submissions are editted. Conflicts with Marco CRM functions so be careful as it will cause duplicates.'),
      '#required' => FALSE,
    ];

    // Properties.
    $form['props'] = [
      '#type' => 'details',
      '#title' => $this->t('Properties'),
      '#description' => $this->t('The property info to send to AutoPlay.'),
      '#open' => TRUE,
    ];

    // There are a bunch of things missing from SaveLead (and perhaps others?)
    // so we always force load them at the top. In the wsclient days
    // we had a UI to add fields manually, but am shortcutting that here.
    // @todo Refactor this when there is time or a bug to solve.
    $overrides = [
      'DealershipId' => ['type' => 'int'],
      'CustomerEmail' => ['type' => 'string'],
      'CustomerFirstName' => ['type' => 'string'],
      'CustomerLastName' => ['type' => 'string'],
      'CustomerPhone' => ['type' => 'string'],
    ];
    // Generate properties.
    $model = $types[$this->configuration['operation']]['request']['type'];
    foreach ($overrides + $types[$model] as $prop => $value) {
      $form['props'][$prop] = [
        '#title' => $prop,
        '#type' => 'textfield',
        '#default_value' => $this->configuration['props'][$prop] ?? '',
        '#description' => $value['type'],
      ];
    }

    $this->elementTokenValidate($form);

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $formState) {
    parent::submitConfigurationForm($form, $formState);

    // Reset all props in case the method has changed.
    $this->configuration['props'] = [];
    // Custom handling for dynamic props.
    $values = $formState->getValues();
    foreach ($values['props'] as $prop => $value) {
      // Don't bother to store empty props.
      if ($value) {
        $this->configuration['props'][$prop] = $value;
      }
    }

    $this->applyFormStateToConfiguration($formState);
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return [
      '#markup' => $this->t('<strong>WSDL:</strong> @conf<br><strong>Method:</strong> @method', ['@conf' => $this->configuration['wsdl'], '@method' => $this->configuration['operation']]),
    ];
  }

  /**
   * Convert metadata about data types provided by a SOAPClient into a wsclient
   * compatible data type array.
   *
   * @param array $types
   *   The array containing the struct strings.
   * @return
   *   A data type array with property information.
   */
  public function parseTypes(array $types) {
    $wsclient_types = [];
    foreach ($types as $type_string) {
      if (strpos($type_string, 'struct') === 0) {
        $parts = explode('{', $type_string);
        // Cut off struct and whitespaces from type name.
        $type_name = trim(substr($parts[0], 6));
        $wsclient_types[$type_name] = [];
        $property_string = $parts[1];
        // Cut off trailing '}'
        $property_string = substr($property_string, 0, -1);
        $properties = explode(';', $property_string);
        // Remove last empty element
        array_pop($properties);
        // Initialize property information.
        foreach ($properties as $property_string) {
          // Cut off white spaces.
          $property_string = trim($property_string);
          $parts = explode(' ', $property_string);
          $property_type = $parts[0];
          $property_name = $parts[1];
          $wsclient_types[$type_name][$property_name] = [
            'type' => $property_type,
          ];
        }
      }
    }
    return $wsclient_types;
  }

  /**
   * Convert metadata about operations provided by a SOAPClient into a
   * compatible operations array.
   *
   * @param array $operations
   *   The array containing the operation signature strings.
   * @return
   *   An operations array with parameter information.
   */
  public function parseOperations(array $operations) {
    $wsclient_operations = [];
    foreach ($operations as $operation) {
      $parts = explode(' ', $operation);
      $return_type = $parts[0];
      $name_parts = explode('(', $parts[1]);
      $op_name = $name_parts[0];
      $wsclient_operations[$op_name] = [
        'result' => $return_type,
      ];
      $parts = explode('(', $operation);
      // Cut off trailing ')'.
      $param_string = substr($parts[1], 0, -1);
      if ($param_string) {
        $parameters = explode(',', $param_string);
        foreach ($parameters as $parameter) {
          $parameter = trim($parameter);
          $parts = explode(' ', $parameter);
          $param_type = $parts[0];
          // Remove leading '$' from parameter name.
          $param_name = substr($parts[1], 1);
          $wsclient_operations[$op_name]['parameter'][$param_name] = [
            'type' => $param_type,
          ];
        }
      }
    }
    return $wsclient_operations;
  }

}
