<?php

namespace Drupal\autoplay\Plugin\QueueWorker;

//use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
//use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\autoplay\AutoplayBase;

/**
 * A Node Publisher that publishes nodes on CRON run.
 *
 * @QueueWorker(
 *   id = "autoplay_submission_handler",
 *   title = @Translation("Autoplay Submission Handler"),
 *   cron = {"time" = 15}
 * )
 */
class AutoplaySubmissionHandler extends QueueWorkerBase {
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    // return $instance;
  }

  public function processItem($data) {
    $sid = $data->sid;
    $config = $data->config;
    \Drupal::logger('autoplay')->notice('Begin queue process for ' . $sid);
    AutoplayBase::handleSubmission($sid, $config);
    \Drupal::logger('autoplay')->notice('End queue process for ' . $sid);
  }
}
