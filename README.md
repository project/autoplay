# Autoplay Auto

This module provides Drupal integration of the Autoplay API primarily via a webform handler and Drupal's queue system to collect data from website users.

Autoplay is a tool for dealerships, dealer groups and automakers to track and manage leads, maximise sales, and streamline processes.

## Features

* Webform integration.
* Drupal queue support.
* Support for all operation provided for in the Autoplay WSDL.
* Dynamic property population from the Autoplay WSLD definitions.
* Ability to override default IDs per webform for testing and debugging.
* Full token support to populate values from webform fields and deeply nested referenced entities.

# Dependencies

The webform module is required by this module as the only point of integration supported is via a webform handler.
